/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2_semana2;

import java.util.Scanner;

/**
 *
 * @author Gerald Villalta R
 */
public class Salarios {

    public static void salario() {
        float horasLaboradas, precioHora, salarioBruto, salarioNeto, deducciones;
        Scanner consulta = new Scanner(System.in);
        System.out.print("\nDigite la cantidad de horas laboradas este mes: ");
        horasLaboradas = consulta.nextFloat();
        System.out.print("Digite el precio por hora: ");
        precioHora = consulta.nextFloat();
        salarioBruto = horasLaboradas * precioHora;
        salarioNeto = (float) (salarioBruto - (salarioBruto * (9.17 / 100)));
        deducciones = salarioBruto - salarioNeto;
        System.out.println("Salario........." + salarioBruto);
        System.out.println("Salario neto...." + salarioNeto);
        System.out.println("Deducicones....." + deducciones);
    }
}
