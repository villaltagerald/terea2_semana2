/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2_semana2;

import java.util.Scanner;

/**
 *
 * @author Gerald Villalta R
 */
public class Tarea2_Semana2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean salir = false;
        Scanner solicitud = new Scanner(System.in);
        int opcion;
        while (!salir) {
            System.out.println("\n***********************************");
            System.out.println("1.Calcular edad actual");
            System.out.println("2.Imprecion de 20 numeros");
            System.out.println("3.Promedio de notas");
            System.out.println("4.Calculo de salario");
            System.out.println("5.Salir");
            System.out.println("***********************************");
            System.out.print("Digitar una opcion: ");
            opcion = solicitud.nextInt();
            switch (opcion) {
                case 1:
                    EdadActual.Edad();
                    break;
                case 2:
                    NumeroHasta20.Veinte();
                    break;
                case 3:
                    PromedioNotas.promedio();
                    break;
                case 4:
                    Salarios.salario();
                    break;
                case 5:
                    System.out.println("\n************************************");
                    System.out.println("GRACIAS POR UTILIZAR NUESTRO SISTEMA");
                    System.out.println("************************************");
                    salir = true;
                    break;
                default:
                    System.out.println("OPCION INVALIDA");
            }
        }
    }

}
