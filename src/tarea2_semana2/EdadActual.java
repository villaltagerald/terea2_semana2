/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2_semana2;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;
import java.util.InputMismatchException;

/**
 *
 * @author Gerald Villalta R
 */
public class EdadActual {

    public static void Edad() {
        int valor1, valor2, valor3, fecha, fechaActual;
        Scanner solicitud = new Scanner(System.in);
        System.out.println("\n****SOLICITUD DE FECHA DE NACIMIENTO****");
        System.out.print("Dia(dd): ");
        valor1 = solicitud.nextInt();
        System.out.print("Mes(MM): ");
        valor2 = solicitud.nextInt();
        System.out.print("Año(yyyy): ");
        valor3 = solicitud.nextInt();
        LocalDate fechaActu = LocalDate.now();
        LocalDate fechaNacimi = LocalDate.of(valor3, valor2, valor1);
        Period periodo = Period.between(fechaNacimi, fechaActu);
        System.out.println("Su edad actual es: " + periodo.getYears() + " años," + periodo.getMonths() + " mes;" + periodo.getDays() + " dias");

    }
}
